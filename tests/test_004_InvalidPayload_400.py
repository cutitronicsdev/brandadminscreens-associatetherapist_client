'''
 file Name - test_0004_InvalidPayload_400
 Date - 16/02/20201
 Test Case - Test valid payload matches existing model
 Jira ticket - N/A
'''


import logging
import json
import jsonschema
import os
from api import associateTherapist_Client
import pytest

logger = logging.getLogger()
logger.setLevel(logging.INFO)


@pytest.fixture()
def lambda_event():
    return {
        "CutitronicsBrandID": '0002',
        "CutitronicsClientID": 'Client6',
        "CutitronicsTherapistID": ['ishga_Therapistjaljsd24', 'ishga_Therapistjalkskdl25', 'ishga_Thera087+pist26']
    }


@pytest.fixture()
def lambda_context():
    return MockContext()


class MockContext:
    def __init__(self):
        self.function_name = "associateTherapist_Client"
        self.log_group_name = '/aws/lambda/associateTherapist_Client'
        self.aws_request_id = '1'
        self.memory_limit_in_mb = 128


def test_handler(lambda_event, lambda_context):

    logger.info(" ")
    logger.info("Valid Payload")
    logger.info(" ")

    lv_result = associateTherapist_Client.lambda_handler(
        lambda_event, lambda_context)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = json.dumps(lv_result)

    lv_schema = open(os.path.join(os.path.dirname(__file__),
                                  'associateTherapist_Client_400_response.json'), 'r').read()
    lv_schema = json.loads(lv_schema)
    try:
        jsonschema.validate(instance=lv_result, schema=lv_schema,
                            format_checker=jsonschema.FormatChecker())
    except jsonschema.exceptions.ValidationError as e:
        print("well-formed but invalid JSON: ", e)
    except json.decoder.JSONDecodeError as e:
        print("poorly-formed text, not JSON: ", e)
