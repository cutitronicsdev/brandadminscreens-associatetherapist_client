'''
 file Name - test_0002_invalidClientID
 Date - 30/02/20201
 Test Case - Test invalid CutitronicsBrandID returns handled error
 Jira ticket - N/A
'''

import logging
import json
from api import associateTherapist_Client

import pytest

logger = logging.getLogger()
logger.setLevel(logging.INFO)


@pytest.fixture()
def lambda_event():
    return {
        "CutitronicsBrandID": '0002',
        "CutitronicsClientID": 'Client6',
        "CutitronicsTherapistID": ['ishga_Therapistjaljsd24', 'ishga_Therapistjalkskdl25', 'ishga_Thera087+pist26']
    }


@pytest.fixture()
def lambda_context():
    return MockContext()


class MockContext:
    def __init__(self):
        self.function_name = "associateTherapist_Client"
        self.log_group_name = '/aws/lambda/associateTherapist_Client'
        self.aws_request_id = '1'
        self.memory_limit_in_mb = 128


def test_handler(lambda_event, lambda_context):

    logger.info("Invalid BrandID, Valid ClientID")

    lv_result = associateTherapist_Client.lambda_handler(
        lambda_event, lambda_context)
    lv_result = json.loads(lv_result.get('body'))

    assert lv_result.get('Status') == "Error"
    assert lv_result.get("ErrorCode") == "associateTherapist_Client_003"
    # assert lv_result.get("ErrorDesc") ==  "Provided  Therapists  have been given permissions by '%(CutitronicsClientID)s'" % { "CutitronicsClientID" : lv_result.get("CutitronicsClientID") }
