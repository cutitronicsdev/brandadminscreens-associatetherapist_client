import logging
import json
import pymysql
import os
import pprint

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'],
                                     os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error(
            "ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {
                           'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(500)
            else:

                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_getTherapists(lv_CutitronicsBrandID):

    lv_list = []
    lv_statement = "SELECT CutitronicsTherapistID, TherapistName FROM TherapistDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(
                lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def fn_checkClientExist(lv_CutitronicsClientID):

    lv_list = []
    lv_statement = "SELECT CutitronicsClientID, Status, Latitude, Longitude, GDPRConsent, FirstName, Surname, EmailAddress, DOB, Gender, FITZP, MedicalConcernsDetails, MedicalInformationDetails, SkinConcerns, Allergies, DeletionFlag FROM ClientDetails WHERE CutitronicsClientID = %(CutitronicsClientID)s AND DeletionFlag IS NULL ORDER BY 1;"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(
                lv_statement, {'CutitronicsClientID': lv_CutitronicsClientID})

            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list[0]

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def fn_clientPermissionCheck(lv_CutitronicsClientID, lv_CutitronicsTherpistID):
    lv_list = []
    lv_statement = "SELECT 'Y' FROM ClientPermission WHERE CutitronicsClientID = %(CutitronicsClientID)s AND CutitronicsTherapistID = %(CutitronicsTherpistID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {
                           'CutitronicsClientID': lv_CutitronicsClientID, 'CutitronicsTherpistID': lv_CutitronicsTherpistID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list.append(dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def fn_associateTherapists_with_Client(lv_CutitronicsBrandID, lv_CutitronicsClientID, lv_CutitronicsTherpistID, lv_ShareDetails=0):

    lv_statement = "INSERT INTO `ClientPermission` (`CutitronicsClientID`, `CutitronicsBrandID`, `CutitronicsTherapistID`, `ShareDetails`, `CreationDate`, `CreationUser`, `LastUpdateDate`, `Attribute1`, `Attribute2`, `Attribute3`) VALUES ( %(CutitronicsClientID)s, %(CutitronicsBrandID)s, %(CutitronicsTherapistID)s, %(ShareDetails)s, SYSDATE() ,NULL, NULL, NULL, NULL, NULL);"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID, 'CutitronicsClientID': lv_CutitronicsClientID,
                                          'CutitronicsTherapistID': lv_CutitronicsTherpistID, 'ShareDetails': lv_ShareDetails})

        if cursor.rowcount == 1:
            return 100

        else:
            logger.warning("Could not INSERT into DataBase")
            Connection.rollback()
            return 500

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def lambda_handler(event, context):
    """
    associateTherapist_Client for a registered client

    """

    openConnection()

    logger.info(
        "Lambda function - {function_name}".format(function_name=context.function_name))

    # Variables

    lv_CutitronicsBrandID = None
    lv_CutitronicsTherapistID = None
    lv_CutitronicsClientID = None

    # Return block

    associateTherapist_Client = {"Service": context.function_name, "Status": "Success",
                                 "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {"CutitronicsBrandID": None}}

    try:

        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                logger.info("Body type exception: %s", e.args)
                body = event
            except TypeError:
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')
        lv_CutitronicsClientID = body.get('CutitronicsClientID')
        lv_CutitronicsTherapistID = body.get('CutitronicsTherapistID')

        '''
        1. Does brand exist ?
        '''

        logger.info(' ')
        logger.info("Testing if Brand ID exists - '{lv_CutitronicsBrandID}'".format(
            lv_CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(' ')

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 500:  # Failure

            logger.error(' ')
            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(
                lv_CutitronicsBrandID=lv_CutitronicsBrandID))
            logger.error(' ')

            associateTherapist_Client['Status'] = "Error"
            # associateTherapist_Client_001
            associateTherapist_Client['ErrorCode'] = context.function_name + "_001"
            associateTherapist_Client['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"
            associateTherapist_Client["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            associateTherapist_Client["ServiceOutput"]["CutitronicsClientID"] = lv_CutitronicsClientID

            # Lambda response

            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(associateTherapist_Client)
            }

        '''
        2. Does Client Exist
        '''

        lv_ClientType = fn_checkClientExist(lv_CutitronicsClientID)

        if lv_ClientType == 300:  # Failure

            print(lv_ClientType)
            print()
            logger.error(" ")
            logger.error("Client '{lv_CutitronicsClientID}' not found in the back office".format(
                lv_CutitronicsClientID=lv_CutitronicsClientID))
            logger.error(" ")

            associateTherapist_Client['Status'] = "Error"
            # viewGuests_out_001
            associateTherapist_Client['ErrorCode'] = context.function_name + "_002"
            associateTherapist_Client['ErrorDesc'] = "Supplied CutitronicsClientID does not exist in the BO Database"
            associateTherapist_Client["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            associateTherapist_Client["ServiceOutput"]["CutitronicsClientID"] = lv_CutitronicsClientID

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(associateTherapist_Client)
            }

        '''
        3.  INSERT Selected Therapists into ClientPermissions
        '''
        therapist_insertcount = 0
        for therapist_inx in range(len(lv_CutitronicsTherapistID)):
            if fn_clientPermissionCheck(lv_CutitronicsClientID, lv_CutitronicsTherapistID[therapist_inx]) == 300:
                fn_associateTherapists_with_Client(
                    lv_CutitronicsBrandID, lv_CutitronicsClientID, lv_CutitronicsTherpistID=lv_CutitronicsTherapistID[therapist_inx])

                therapist_insertcount += 1

        if therapist_insertcount != 0:

            # Lambda response

            # Create output

            Connection.commit()
            Connection.close()

            associateTherapist_Client['Status'] = "Success"
            # viewGuests_out_001
            associateTherapist_Client['ErrorCode'] = context.function_name + "_000"
            associateTherapist_Client['ErrorDesc'] = ""
            associateTherapist_Client["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            associateTherapist_Client["ServiceOutput"]["CutitronicsClientID"] = lv_CutitronicsClientID

            return {
                "isBase64Encoded": "false",
                "statusCode": 200,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(associateTherapist_Client)
            }

        else:
            logger.error(' ')
            logger.error("All Therapists have already been assigned to '{lv_CutitronicsClientID}'".format(
                lv_CutitronicsClientID=lv_CutitronicsClientID))
            logger.error(' ')

            associateTherapist_Client['Status'] = "Error"
            # getAllTherapist_out_001
            associateTherapist_Client['ErrorCode'] = context.function_name + "_003"
            associateTherapist_Client['ErrorDesc'] = "Provided  Therapists  have been given permissions by '%(CutitronicsClientID)s'" % {
                "CutitronicsClientID": lv_CutitronicsClientID}

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(associateTherapist_Client)
            }
    except Exception as e:
        logger.error(
            "CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        # Populate Cutitronics reply block

        associateTherapist_Client['Status'] = "Error"
        # regUser_000
        associateTherapist_Client['ErrorCode'] = context.function_name + "_000"
        associateTherapist_Client['ErrorDesc'] = "CatchALL"
        associateTherapist_Client['ServiceOutput']['brandID'] = lv_CutitronicsBrandID

        # Lambda Exeution has  failed

        Connection.rollback()
        Connection.close()

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(associateTherapist_Client)
        }
